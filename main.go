package main

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/junnotantra/sentry-test/internal/app/senty-test/routes"
	raven "github.com/getsentry/raven-go"
)

func main() {
	log.Printf("my pid is %v\n", os.Getpid())
	log.Printf("port: " + os.Getenv("PORT"))
	log.Println("go!")

	raven.SetDSN("https://f04f5743a8ed470abc4c40622def7071:294d647ed1744d6ea3c43bd12d7727b2@sentry.io/1240264")
	raven.SetEnvironment("production")
	r := routes.InitRoutes()
	http.ListenAndServe(":"+os.Getenv("PORT"), r)

}
