var dataCacheName = 'sentry-test-v2';
var cacheName = 'sentry-test-final-2';
var filesToCache = [
  '/',
  '/journals/add',
  '/manifest.json',
  '/images/android-desktop.png',
  '/images/ios-desktop.png',
  '/images/touch/ms-touch-icon-144x144-precomposed.png',
  '/images/favicon.png',
  '/css/fonts.css',
  '/css/material.min.css',
  '/css/sentry-test.css',
  '/js/moment.min.js',
  '/js/popper.min.js',
  '/js/bootstrap.min.js',
  '/js/material.min.js',
  '/js/jquery-3.3.1.min.js',
  '/images/icons/icon-128x128.png',
  '/images/icons/icon-144x144.png',
  '/images/icons/icon-152x152.png',
  '/images/icons/icon-192x192.png',
  '/images/icons/icon-256x256.png',
  '/images/icons/icon-512x512.png'
];

self.addEventListener('install', function(e) {
  console.log('[ServiceWorker] Install');
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      console.log('[ServiceWorker] Caching app shell');
      return cache.addAll(filesToCache);
    })
  );
});

self.addEventListener('activate', function(e) {
  console.log('[ServiceWorker] Activate');
  e.waitUntil(
    caches.keys().then(function(keyList) {
      return Promise.all(keyList.map(function(key) {
        if (key !== cacheName && key !== dataCacheName) {
          console.log('[ServiceWorker] Removing old cache', key);
          return caches.delete(key);
        }
      }));
    })
  );
  /*
  * Fixes a corner case in which the app wasn't returning the latest data.
  * You can reproduce the corner case by commenting out the line below and
  * then doing the following steps: 1) load app for first time so that the
  * initial New York City data is shown 2) press the refresh button on the
  * app 3) go offline 4) reload the app. You expect to see the newer NYC
  * data, but you actually see the initial data. This happens because the
  * service worker is not yet activated. The code below essentially lets
  * you activate the service worker faster.
  */
  return self.clients.claim();
});

self.addEventListener('fetch', function(event) {
  var requestURL = new URL(event.request.url);
  if(filesToCache.includes(requestURL.pathname)){
    event.respondWith(
      caches.open(cacheName).then(function(cache) {
        return cache.match(event.request).then(function(response) {
          var fetchPromise = fetch(event.request).then(function(networkResponse) {
            cache.put(event.request, networkResponse.clone());
            return networkResponse;
          })
          return response || fetchPromise;
        })
      })
    );
  }
});
