loadJournals(1);

function loadJournals(page) {
    $.ajax({
        url: "/api/v1/journal-listing",
        method: "get",
        data: {
            page: page
        }
    }).done(function (result) {
        var data = JSON.parse(result);
        for (var i = 0, len = data.length; i < len; i++) {
            $('#journals-listing').append(createCard(data[i]))
          }
    });
}

function createCard(data){
    return '<a href="/journals/'+data.ID+'" class="list-group-item list-group-item-action flex-column align-items-start">'+
        '<div class="d-flex w-100 justify-content-between">'+
        '<h5 class="mb-1"><b class="text-secondary">'+data.Title+'</b></h5>'+
        '<small class="text-muted">'+moment(data.CreatedAt).format('LL')+'</small>'+
      '</div>'+
      '<p class="mb-1">'+createSpoiler(data.Content, 50)+'</p>'+
    '</a>';
}

function createSpoiler(text, count){
    return text.slice(0, count) + (text.length > count ? "..." : "");
}