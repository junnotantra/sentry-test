(function() {
  'use strict';

  // service worker code here
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker
             .register('/sw.js', {scope: '/'})
             .then(function() { console.log('Service Worker Registered'); })
             .catch(function(error) { console.log('Service worker registration failed, error:', error);
  });
;
  }
})();
