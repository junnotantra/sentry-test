ts := $(shell /bin/date "+%Y-%m-%d %H:%M:%S")
export PORT = 8080

build-app:
	@echo $(ts) "=== BUILDING APP ==="
	@go build -o sentry-test ./

run-app: build-app
	@echo $(ts) "=== RUNNING APP ==="
	@./sentry-test