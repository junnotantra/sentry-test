package routes

import (
	"html/template"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"bitbucket.org/junnotantra/sentry-test/internal/app/senty-test/handlers"
	"bitbucket.org/junnotantra/sentry-test/internal/app/senty-test/middlewares"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

var templates = template.Must(template.ParseGlob("web/templates/*"))

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

func InitRoutes() *chi.Mux {
	r := chi.NewRouter()

	// A good base middleware stack
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middlewares.HttpsRedirectMiddleware)

	// Set a timeout value on the request context (ctx), that will signal
	// through ctx.Done() that the request has timed out and further
	// processing should be stopped.
	r.Use(middleware.Timeout(60 * time.Second))

	r.Get("/", handlers.SentryErrorHandler)

	r.Mount("/errors", errorRouter())

	// Static files server
	workDir, _ := os.Getwd()
	filesDir := filepath.Join(workDir, "web/resources")
	fileServer(r, "/", http.Dir(filesDir))

	return r
}
