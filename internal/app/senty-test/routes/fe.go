package routes

import (
	"net/http"
	"strings"

	"bitbucket.org/junnotantra/sentry-test/internal/app/senty-test/handlers"
	"github.com/go-chi/chi"
)

func errorRouter() http.Handler {
	r := chi.NewRouter()
	// TODO: add auth middleware

	r.Get("/", handlers.SentryErrorHandler)
	r.Get("/error1", handlers.SentryError2Handler)

	return r
}

func fileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit URL parameters.")
	}

	fs := http.StripPrefix(path, http.FileServer(root))

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fs.ServeHTTP(w, r)
	}))
}
