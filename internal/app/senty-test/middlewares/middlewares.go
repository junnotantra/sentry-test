package middlewares

import (
	"net/http"
	"os"
)

func HttpsRedirectMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if os.Getenv("DEPLOY_ENV") == "heroku" && r.Header.Get("x-forwarded-proto") != "https" {
			http.Redirect(w, r, "https://"+r.Host+r.URL.String(), 302)
		} else {
			next.ServeHTTP(w, r)
		}
	})
}
