package types

import (
	"time"

	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	UserName         string
	Email            string
	FullName         string
	Salt             string
	Password         string
	LastLogin        time.Time
	Locked           bool
	FailedLoginCount int
}
