package types

import (
	"bitbucket.org/junnotantra/sentry-test/internal/pkg/utils"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Journal struct {
	gorm.Model
	Title   string
	Content string
}

func GetJournalByID(id int) (Journal, error) {
	db, err := gorm.Open(utils.DB_DLCT, utils.DB_CONS)
	utils.HandleError(err)
	defer db.Close()

	var journal Journal
	db.First(&journal, id)

	return journal, err
}

func NewJournal(title, content string) error {
	db, err := gorm.Open(utils.DB_DLCT, utils.DB_CONS)
	utils.HandleError(err)
	defer db.Close()

	journal := Journal{
		Title:   title,
		Content: content,
	}
	db.Create(&journal)

	return err
}

func EditJournal(id int, title, content string) error {
	db, err := gorm.Open(utils.DB_DLCT, utils.DB_CONS)
	utils.HandleError(err)
	defer db.Close()

	var journal Journal
	db.First(&journal, id)

	journal.Title = title
	journal.Content = content

	db.Save(&journal)

	return err
}

func DeleteJournal(id int) error {
	db, err := gorm.Open(utils.DB_DLCT, utils.DB_CONS)
	utils.HandleError(err)
	defer db.Close()

	var journal Journal
	db.First(&journal, id)
	db.Delete(&journal)

	return err
}

func GetJournals(page int) ([]Journal, error) {
	db, err := gorm.Open(utils.DB_DLCT, utils.DB_CONS)
	utils.HandleError(err)
	defer db.Close()

	offset := (page - 1) * 10

	var journals []Journal
	db.Limit(10).Offset(offset).Order("id desc").Find(&journals)

	return journals, err
}
