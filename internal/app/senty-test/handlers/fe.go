package handlers

import (
	"html/template"
)

type BasicPageData struct {
	Title         string
	ScriptInclude string
}

var Templates = template.Must(template.ParseGlob("web/templates/*"))
