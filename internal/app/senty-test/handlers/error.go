package handlers

import (
	"net/http"
	"strconv"

	"bitbucket.org/junnotantra/sentry-test/internal/pkg/utils"
)

func SentryErrorHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	err := Templates.ExecuteTemplate(w, "journalsListing", "error")
	utils.HandleError(err)
}

func SentryError2Handler(w http.ResponseWriter, r *http.Request) {
	_, err := strconv.Atoi("b")
	utils.HandleError(err)
	w.WriteHeader(http.StatusOK)
}
