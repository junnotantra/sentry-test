package utils

import (
	"runtime"
	"strings"

	raven "github.com/getsentry/raven-go"
	log "github.com/sirupsen/logrus"
)

// HandleError logs the line number and optionaly function name.
// Return true if got error and handled.
// Return false if no error.
func HandleError(err error, verbose ...bool) bool {
	if err != nil {
		raven.CaptureErrorAndWait(err, nil)
		if len(verbose) > 0 && verbose[0] {
			// notice that we're using 1, so it will actually log the where
			// the error happened, 0 = this function, we don't want that.
			pc, fn, line, _ := runtime.Caller(1)
			i := strings.Index(fn, "sentry-test")
			if i < 1 {
				i = 1
			}
			fn = fn[i+9:]

			log.WithFields(
				log.Fields{
					"process":  pc,
					"filename": fn,
					"line":     line,
				}).Error(err.Error())
			return true
		}

		// notice that we're using 1, so it will actually log where
		// the error happened, 0 = this function, we don't want that.
		_, fn, line, _ := runtime.Caller(1)
		i := strings.Index(fn, "disposisi")
		if i < 1 {
			i = 1
		}
		fn = fn[i+9:]

		log.WithFields(
			log.Fields{
				"filename": fn,
				"line":     line,
			}).Error(err.Error())
		return true
	}
	return false
}
